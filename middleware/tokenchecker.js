const jwt = require('jsonwebtoken')
const userManager = require('../managers/UserManager')
const keys  = require('../config/keys')

const tokenChecker = async(req, res, next) => {
    try{
        const token = req.header('Authorization').replace('Bearer ', '');
        // console.log(token);
        const ver = await jwt.verify(token, keys.jwtKEY.random);
        console.log(ver);  
        next()
    } catch(error){
        res.status(401).send({
            error : 'Unauthorized'
        });
    }
} 
module.exports = tokenChecker;
