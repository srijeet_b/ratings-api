const User = require('../models/User')
const jwt = require('jsonwebtoken')
const keys = require('../config/keys')
const bcrypt = require('bcryptjs')

class UserManager {
    constructor(){

    }
    // async getUserDetailsWithPosts(id){
    //     const userDetailsWithPosts = await User.findOne({id : id}).populate('posts')
    //     .exec((err, users) => {
    //         if(err){
    //             throw err;
    //         }
    //         console.log(userDetailsWithPosts);
    //     });
    // } 
    async getAllUserDetails(){
        const allUsers = await User.find();
        return allUsers;
        // console.log(allUsers);
    }
    async getUserById(inpid){
        const user = await User.find({_id : inpid});
        return user;
    }
    async createUser(name, email, password){
        const user = new User({
            name: name,
            email : email,
            password : password
        });
        const resp = await user.save();
        return resp;
        // const token = await this.generateAuthToken(resp._id);
        // // console.log(token);
        // const newUpdate = await this.updateUserDetails(resp._id, {token : token});
        // return [resp, newUpdate];
        // console.log(resp);
    }
    async updateUserDetails(id, updateParams){
        const userUpdates = User.updateOne({_id : id}, updateParams, (err, docs) => {
            if(err){
                throw err;
            }
        });
        return userUpdates;
        
    }
    deleteUser(id){
        const user = User.findOne({id: id});
        User.deleteOne(user, (err) => {
            throw err;
        });
        return true;
    }
    async generateAuthToken(inpid){
        const user = await this.getUserById(inpid);
        //token for 3 hrs
        const token = jwt.sign({user}, keys.jwtKEY.random, {expiresIn : 60*60*3})
        return token;
    }
    async matchUserCredentials(email, password){
        const user = await User.findOne({email});
        if(!user){
            throw new Error('Email Not Found');
        }
        const isPasswordMatch = await new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                if(err){
                    reject(err);
                }
                resolve(res);
            });
        });
        
        if(!isPasswordMatch){
            throw new Error('Password Does not Match with Email');
        }
        // passing all cred check
        return user;
    }
}

module.exports = new UserManager;