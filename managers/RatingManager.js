const Rating = require('../models/Rating')
const jobManager = require('../models/Job')
const userManager = require('../managers/UserManager')

class RatingManager {
    // constructor(User, Job){
    //     this.User = User._id;
    //     this.Job = Job.jobType;
    // }
    // getRatingsWithAuthorAndJobs(inp){
    //     return Rating.findOne({id: inp})
    //     .populate(['author', 'jobId']).exec((err, docs) => {
    //         if(err){
    //             throw err;
    //         }
    //         console.log(docs);
    //     });   
    // }
    async getAllRatings(){
        const ratings = await Rating.find();
        return ratings;
    }
    async getRatingById(inpid){
        const rating = await Rating.findById({_id : inpid});
        return rating;
    }
    async createRating(jobId, score, comment){
        // const user = await userManager.getUserById(req.user._id);
        const job = await jobManager.getJobById(jobId);
        if(job._id === req.user){
            const rating = new Rating({
                ratingScore : score,
                ratingComment : comment
            });
            const res = await rating.save(); // after saving 
            const updateParams = {res}; //got id from response and passed to update job
            await jobManager.updateJobDetails(jobId, updateParams)
            
            return res;
        }
    }
    // updateRatingDetails(id, score, comment){
    // }
    deleteRating(id){
        const deleteRating = Rating.findByIdAndDelete(id, (err, docs) => {
            if(err){
                throw err;
            }
            console.log(docs);
        });
        return deleteRating;
    }
}

module.exports = new RatingManager