const Job = require('../models/Job')
const User = require('../models/User')

class JobManager {
    // constructor(User){
    //     this.User = User.id;
    // }
    async getJobDetailsWithRatings(inpid){
        const res = await User.findOne({id : inpid})
        .populate('ratings').exec((err, jobs) => {
            if(err) {
                throw err
            }
            console.log("details", jobs);
        });
        return res;
    }
    async getAllJobs(){
        const allJobs = await Job.find()
        console.log(allJobs);
        return allJobs;
    }
    async getJobById(inpid){
        const job = await Job.findById({_id: inpid});
        return job;
    }
    async createJobs(name, type, desc){
        const job = new Job({
            name: name,
            jobType: type, 
            jobDescription : desc
        });
        const result = await job.save();
        console.log(result);
        return result;
    }
    updateJobDetails(inpid, updateParams){
        const jobUpdates = Job.updateOne({_id : inpid}, updateParams , (err, docs) => {
            if(err){
                throw err;
            }
        });
        return jobUpdates;
    }
    deleteJob(inpid){
        const job = Job.findOne({_id: inpid});
        Job.deleteOne(job, (err) => {
            throw err;
        });
        return true;
    }
}


module.exports  = new JobManager;