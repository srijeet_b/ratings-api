const express = require('express')
const validator = require('validator')
const router = express.Router()
const Job = require('../models/Job')
const User = require('../models/User')
const Rating = require('../models/Rating')
const tokenchecker = require('../middleware/tokenchecker')

const userManager = require('../managers/UserManager')
const jobManager = require('../managers/JobManager')
const ratingManager = require('../managers/RatingManager')
// user create
router.post('/users' ,async(req, res) => {
    try{
        const {name, email, password } = req.body
        const user = await userManager.createUser(name, email, password);
        res.status(200).json(user)
    }
    catch(error) {
        res.status(400).send({error : error.message})
    }
})
// user login
router.post('/users/login', async(req, res) => {
    try{
        const {email, password} = req.body
        const user = await userManager.matchUserCredentials(email, password)
        if(!user){
            return res.status(401).send({error: 'login failed'})
        }
        console.log(user);
        const token = await userManager.generateAuthToken(user._id);
        console.log(token);
        const userId = user._id;
        res.status(200).json({userId, token});
    }
    catch(error) {
        res.status(400).json({error: error.message})
    }
})
// req to get all users details 
router.get('/usersAll',tokenchecker, async(req, res) => {
    try{
        const getUsers = await userManager.getAllUserDetails();
        res.status(200).json(getUsers);
    }
    catch(error) {
        res.status(400).json({error : error.message});
    }
})
// if user logs in req data -- middleware needed
router.get('/users/details/:id',tokenchecker, async(req, res) => {
    try{
        const user = await userManager.getUserById(req.params.id);
        res.status(200).json(user);
    }catch(error){
        res.status(400).json({error: error.message});
    }
});


// <===============================>

//get all Jobs HTTP GET JOBS
router.get('/joblisting',tokenchecker,  async(req, res) => {
    try{
        const jobs = await jobManager.getAllJobs()
        res.status(200).json(jobs)
    }catch (error){
        res.status(500).json({error : error.message})
    }
})
// get for particular job
router.get('/joblisting/:id',tokenchecker, async(req, res) => {
    try{
        const job = await jobManager.getJobById(req.params.id);
        res.status(200).json(job);
    }catch(error){
        res.status(400).json({error: error.message})
    }
})
// // update jobs for a particular id 
// router.put('/joblisting/:id', async (req, res) => {
//     try{
//     const updateparams = req.body;
//     const updateJob = await jobManager.updateJobDetails(req.params.id, updateparams)
//     res.status(200).json(updateJob);
//     }catch(error){
//         res.status(400).json({error : error.message});
//     }
// })

// job HTTP get for PARTICULAR job
// router.get('/joblisting/:id', async(req, res) => {
//     try{
//         const job = await jobManager.getJobDetailsWithRatings(req.params.id)
//         res.status(200).json(job)
//     }catch (error){
//         res.status(500).json({error : error.message})
//     }
// })

// <===============================>

//GET request for ratings 
router.get('/ratings',tokenchecker,  async(req, res) => {
    try {
        const ratings = await ratingManager.getAllRatings();
        res.status(200).json(ratings)
    } catch (error) {
        res.status(500).json({error : error.message})
    }
})

// CREATION of RATING 
router.post('/job/:jobid/rating',tokenchecker, async(req, res) => {
    try{
        const {score, comment}  = req.body;
        const rating = await ratingManager.createRating(req.params.jobid, score, comment);
        res.status(200).json(rating);
    }
    catch(error) {
        res.status(400).json({error : error.message});
    }
})

// fetch details of single ratings id
router.get('/ratings/:id',tokenchecker,  async(req, res) => {
    try{
        const rating = await ratingManager.getRatingById(req.params.id)
        res.status(200).json(rating)
    }catch (error){
        res.status(500).json({error : error.message})
    }
})

// router.delete('/ratings/:id', async(req, res) => {
//     try{
//         const rating = await Rating.deleteRating(req.params.id);
//         res.status(200).json(rating);
//     }catch(error) {
//         res.status(400).json({error: error.message})
//     }
// });

module.exports = router

