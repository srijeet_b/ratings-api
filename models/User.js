const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
// const jwt = require('jsonwebtoken')
const keys = require('../config/keys')

const userSchema = mongoose.Schema({
    name: {
        type : String, 
        required  : true,
        trim : true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: value => {
            if(!validator.isEmail(value)){
                throw new Error({error : 'Invalid email address'})
            }
        }
    },
    password: {
        type: String, 
        required: true,
        minLength: 5
    },
    token: {
        type: String
    }
})
userSchema.pre('save',async function(next)  {
    const user = this;
    const pass = await new Promise((resolve, reject) => {
        bcrypt.hash(user.password, keys.hashingCreds.salt, function(err, hash) {
        if(err){
            reject(err);
        }
        resolve(hash);
        }); 
    });
    user.password = pass;
    next();
});


const User = mongoose.model('User', userSchema)
module.exports = User