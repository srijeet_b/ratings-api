const mongoose = require('mongoose')
const validator = require('validator')

const ratingSchema = mongoose.Schema({
    ratingScore: {
        type : Number,
        required : true,
        min : 1,
        max : 5 
    },
    ratingComment: {
        type : String
    },
    // a rating should have Only One User and Only One Rating attached to it
    author: {
        type:  mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    jobId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Job'
    }
})

const rating = mongoose.model('Rating', ratingSchema)
module.exports = rating