const mongoose = require('mongoose')
const validator = require('validator')

const jobSchema = mongoose.Schema({
    name: {
        type : String,
        required : true,
        trim: true
    },
    jobType: {
        type : String,
        required : true,
        trim : true
    },
    jobDescription: {
        type : String
    },
    customer : {
        type : mongoose.Schema.Types.ObjectId, 
        required: true,
        ref: 'User'
    },
    // a job can have multiple rating
    ratings: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Rating'
    }
})

const job = mongoose.model('Job', jobSchema)
module.exports = job