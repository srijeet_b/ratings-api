const express = require('express')
const mongoose = require('mongoose')
const keys = require('./config/keys')
const apiRoutes = require('./routes/routes.js')

const bodyparser = require('body-parser')

const app = express()
const port = process.env.PORT || 3000

app.use(express.json())
app.use(bodyparser.urlencoded({extended : false}))
// app.set('view engine', 'ejs')
mongoose.connect(keys.mongo.dbURL,{useNewUrlParser : true, useUnifiedTopology: true}, () => {
    console.log("mongo connected")
})

app.use('/api', apiRoutes)



app.listen(port, () => console.log(`running on ${port}`))